
package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import logica.Accion;
import logica.Usuario;

public class AccionDao {
    private Conexion con;
    
    public AccionDao() {
        this.con = new Conexion();
    }
    
    public Accion getAccionById(int idAccion){
        Accion a;
        Connection accesoBD=con.getConexion();
        String sql = "SELECT * FROM accion WHERE id="+idAccion;
        
        try {
            //System.out.println(sql);
            Statement st = accesoBD.createStatement();
            ResultSet rs=st.executeQuery(sql);
            
            if (rs.first()) {
                int id=rs.getInt("id");
                String nombre=rs.getString("nombre");
                a=new Accion(id,nombre);
                return a;
            }else{
                return null;
            }
            
        } catch (Exception e) {
            System.out.println();
            System.out.println("Error al obtener usuario");
            e.printStackTrace();
            return null;
        }
        
    }
}
