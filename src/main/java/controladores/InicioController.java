package controladores;

import dao.AccionDao;
import dao.CategoriaDao;
import dao.ProductoDao;
import dao.RegistroDao;
import dao.UsuarioDao;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;
import logica.Categoria;
import logica.Producto;
import logica.Registro;
import logica.Usuario;
import vistas.VistaInicio;
import vistas.VistaLogin;
import vistas.VistaProductos;
import vistas.VistaRegistros;

public class InicioController implements ActionListener {

    private VistaInicio vInicio;
    private VistaLogin vLogin;

    public InicioController(VistaInicio vInicio, VistaLogin vLogin) {
        this.vInicio = vInicio;
        this.vLogin = vLogin;
        this.vInicio.getBtnProductos().addActionListener(this);
        this.vInicio.getBtnRegistros().addActionListener(this);
    }

//    public void productos(VistaProductos v) {
//        DefaultTableModel dtf = (DefaultTableModel) v.getjTable1().getModel();
//        ProductoDao productosBD = new ProductoDao();
//        CategoriaDao categoriaBD=new CategoriaDao();
//        ArrayList<Producto> productos = productosBD.getProuctos();
//
//        for (int i = 0; i < productos.size(); i++) {
//            String[] fila = {productos.get(i).getNombre(), productos.get(i).getDescripcion(), Integer.toString(productos.get(i).getCantidad()), categoriaBD.categoriaById(productos.get(i).getCategoria_id()).getNombre()};
//            dtf.addRow(fila);
//        }
//
//    }
    
    public void registros(VistaRegistros v) {
        DefaultTableModel dtf = (DefaultTableModel) v.getjTableRegistros().getModel();
        RegistroDao registrosBD = new RegistroDao();
        ProductoDao productoBD=new ProductoDao();
        UsuarioDao usuarioDao=new UsuarioDao();
        AccionDao accionDao=new AccionDao();
        
        ArrayList<Registro> registros = registrosBD.getRegistros();

        for (int i = 0; i < registros.size(); i++) {
            String[] fila = {registros.get(i).getFecha(),productoBD.getProductoById(registros.get(i).getProducto_id()).getNombre(),usuarioDao.getUsuarioById(registros.get(i).getUsuario_id()).getNombre(),accionDao.getAccionById(registros.get(i).getAccion_id()).getNombre(),Integer.toString(registros.get(i).getCantidad())};
            dtf.addRow(fila);
        }

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (this.vInicio.getBtnProductos() == e.getSource()) {
            VistaProductos v = new VistaProductos(this.vInicio);
            v.productosBD();
//            productos(v);
            v.setVisible(true);
            this.vInicio.setVisible(false);
        }else if(this.vInicio.getBtnRegistros()== e.getSource()){
            VistaRegistros v=new VistaRegistros(this.vInicio);
            registros(v);
            v.setVisible(true);
            this.vInicio.setVisible(false);
        }
    }

}
