
package controladores;

import dao.CategoriaDao;
import dao.ProductoDao;
import dao.RegistroDao;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import javax.swing.JOptionPane;
import logica.Producto;
import logica.Registro;
import vistas.VistaCambiarCantidadProducto;
import vistas.VistaProductos;

public class CambiarCantidadProductoController implements ActionListener {
    private VistaCambiarCantidadProducto vCambiarCantidadProducto;
    private VistaProductos vProductos;

    public CambiarCantidadProductoController(VistaCambiarCantidadProducto vCambiarCantidadProducto, VistaProductos vProductos) {
        this.vCambiarCantidadProducto = vCambiarCantidadProducto;
        this.vProductos = vProductos;
        this.vCambiarCantidadProducto.getBtnGuardarr().addActionListener(this);
        this.vCambiarCantidadProducto.getBtnCancelar().addActionListener(this);
    }
    
    public int prouctoBD(){
        int posicion=this.vCambiarCantidadProducto.getPosicion();
        Producto productoBD=this.vProductos.productosBD().get(posicion);

        CategoriaDao categoriaBD=new CategoriaDao();
        
        this.vCambiarCantidadProducto.getTfNombre().setText(productoBD.getNombre());
        this.vCambiarCantidadProducto.getTfDescripcion().setText(productoBD.getDescripcion());
        this.vCambiarCantidadProducto.getTfCantidad().setText(""+productoBD.getCantidad());
        this.vCambiarCantidadProducto.getTfCategoria().setText(categoriaBD.categoriaById(productoBD.getCategoria_id()).getNombre());
        
        return productoBD.getCantidad();
    }
    
    public Producto productoBDInfo(){
        int posicion=this.vCambiarCantidadProducto.getPosicion();
        Producto productoBD=this.vProductos.productosBD().get(posicion);
        return productoBD;
    }
    
    public void insertarRegistro(Registro r) {
        RegistroDao registroBD = new RegistroDao();
        registroBD.insertRegistro(r);
    }
    
    public String fechaActual(){
        java.util.Date fecha = new Date();
        return fecha+"";
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        
        
        if (this.vCambiarCantidadProducto.getBtnGuardarr() == e.getSource()) {
            
            ProductoDao productoDao=new ProductoDao();
            Producto productoBD=productoBDInfo();
            
            int cantidadNueva=Integer.parseInt(vCambiarCantidadProducto.getTfCantidad().getText());
            int cantidadBD=productoBD.getCantidad();
            
            if (cantidadNueva==cantidadBD) {
                JOptionPane.showMessageDialog(null,"Ingrese una cantidad diferente a la actual");
            }else if(cantidadNueva>cantidadBD){
                productoDao.alterarCantidad(cantidadNueva, productoBD.getId());
                JOptionPane.showMessageDialog(null,"Se aumento la cantidad del producto");
                
                int idUsuario = vProductos.getvInicio().getVLogin().UsuarioLogeado().getId();
                Registro r = new Registro(fechaActual(), productoBD.getId(), idUsuario, 3, cantidadNueva);
                insertarRegistro(r);
                
                this.vProductos.productosBD();
                this.vProductos.setVisible(true);
                this.vCambiarCantidadProducto.dispose();
            }else if(cantidadNueva<cantidadBD){
                productoDao.alterarCantidad(cantidadNueva, productoBD.getId());
                JOptionPane.showMessageDialog(null,"Se disminuyo la cantidad del producto");
                
                int idUsuario = vProductos.getvInicio().getVLogin().UsuarioLogeado().getId();
                Registro r = new Registro(fechaActual(), productoBD.getId(), idUsuario, 4, cantidadNueva);
                insertarRegistro(r);
                
                this.vProductos.productosBD();
                this.vProductos.setVisible(true);
                this.vCambiarCantidadProducto.dispose();
            }
        }else if(this.vCambiarCantidadProducto.getBtnCancelar() == e.getSource()){
            this.vProductos.setVisible(true);
            this.vCambiarCantidadProducto.dispose();
        }
    }
    
    
}
