
package controladores;

import dao.UsuarioDao;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import logica.Usuario;
import vistas.VistaInicio;
import vistas.VistaLogin;

public class LoginController implements ActionListener {
    
    private VistaLogin vLogin;
    private VistaInicio vInicio;
    public LoginController(VistaLogin vLogin) {
        this.vLogin = vLogin;
        this.vInicio=new VistaInicio(vLogin);
        this.vLogin.getBtnIniciar().addActionListener(this);
    }
    
    public Usuario UsuarioLogeado(){
        String user=vLogin.getTfCorreo().getText();
        String pass=new String(vLogin.getTfContra().getPassword());
        
        UsuarioDao d=new UsuarioDao();
        
        Usuario userBD=d.getUsuarioByUser(user);
        
        return userBD;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String user=vLogin.getTfCorreo().getText();
        String pass=new String(vLogin.getTfContra().getPassword());
        
        UsuarioDao d=new UsuarioDao();
        
        Usuario userBD=d.getUsuarioByUser(user);
        
        if (userBD!=null && userBD.getClave().equals(pass)) {

            vInicio.setVisible(true);
            this.vLogin.setVisible(false);
        }
    }
    
}
